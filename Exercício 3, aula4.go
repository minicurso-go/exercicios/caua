package main
import "fmt"

func media( x , y, z, w float64) float64{
    return (x + y + z + w)/4
}

func main() {
    var n1 float64
    var n2 float64
    var n3 float64
    var n4 float64
    
    fmt.Print("Digite a primeira nota:")
    fmt.Scan(&n1)
    fmt.Print("Digite a segunda nota:")
    fmt.Scan(&n2)
    fmt.Print("Digite a terceira nota:")
    fmt.Scan(&n3)
    fmt.Print("Digite a quarta nota:")
    fmt.Scan(&n4)
    
    fmt.Print("Média:", media(n1,n2,n3,n4))
package main
import "fmt"

func main() {
  var idades  = [5]int { 16, 17, 26, 48, 47}//-> arrays possuem tamanhos definidos
  fmt.Println(idades , "Quantas pessoas:", len(idades))
  
  //slices
  var nomes = []string {"Cauã" , "Clara" , "Alan Pedro" , "Carla" , "Alan"}//->Não possuem tamnhos definidos
 
  nomes[0] = "Mendes"//-> Mudar a posição
  nomes = append( nomes , "Cauã")//-> Adicionar 
   fmt.Print("Lista de nomes:" , nomes ,  "Quantos nomes inscritos:" , len(nomes))
}